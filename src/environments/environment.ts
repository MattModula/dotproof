// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBMxJ4alJ9p7qDN4SxvBzEur0wxB0lN80o',
        authDomain: 'dotproof-344c2.firebaseapp.com',
        databaseURL: 'https://dotproof-344c2.firebaseio.com',
        projectId: 'dotproof-344c2',
        storageBucket: 'dotproof-344c2.appspot.com',
        messagingSenderId: '200422123542'
    }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
