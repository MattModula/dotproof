import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { finalize } from 'rxjs/internal/operators';

import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

    private db = firebase.firestore();

    // Image capture
    ref: any;
    task: any;
    frontUploadPercent: any;
    backUploadPercent: any;
    downloadFrontURL: any;
    downloadBackURL: any;

    // Image display
    public frontImages: any;
    public backImages: any;
    public testimg: any;

    constructor(private location: Location,
                private afs: AngularFirestore,
                private afStorage: AngularFireStorage) { }

    ngOnInit() {
        this.db.collection('frontCameraImages').onSnapshot(querySnapshot => {
            let tempFI = [];
            querySnapshot.forEach(function(doc) {
                tempFI.push(doc.data().downloadURL);
            });
            this.frontImages = tempFI;
        });
        this.db.collection('backCameraImages').onSnapshot(querySnapshot => {
            let tempBI = [];
            querySnapshot.forEach(function(doc) {
                tempBI.push(doc.data().downloadURL);
            });
            this.backImages = tempBI;
        });
    }

    goBack() {
        this.location.back();
    }

    uploadFrontFile(event) {
        const file = event.target.files[0];
        const filePath = Math.random().toString(36).substring(2);
        const fileRef = this.afStorage.ref(filePath);
        const task = this.afStorage.upload(filePath, file);
        this.frontUploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(finalize(() => {
            this.downloadFrontURL = fileRef.getDownloadURL();
            this.downloadFrontURL.subscribe(url => {
                this.afs.collection(`frontCameraImages`).add({
                    name: filePath,
                    downloadURL: url
                });
            });
        })).subscribe();
    }

    uploadBackFile(event) {
        const file = event.target.files[0];
        const filePath = Math.random().toString(36).substring(2);
        const fileRef = this.afStorage.ref(filePath);
        const task = this.afStorage.upload(filePath, file);
        this.backUploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(finalize(() => {
            this.downloadBackURL = fileRef.getDownloadURL();
            this.downloadBackURL.subscribe(url => {
                this.afs.collection(`backCameraImages`).add({
                    name: filePath,
                    downloadURL: url
                });
            });
        })).subscribe();
    }

}
