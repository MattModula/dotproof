import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-geolocation',
  templateUrl: './geolocation.component.html',
  styleUrls: ['./geolocation.component.scss']
})
export class GeolocationComponent implements OnInit {

    public map;
    public latlngBounds;

    currentLat: number;
    currentLong: number;
    maxZoom = 2;
    positions = [];

    // For routing
    public origin: any;
    public destination: any;

    constructor(private location: Location) { }

    ngOnInit() { }

    goBack() {
        this.location.back();
    }

    mapReady(gMap) {
        this.map = gMap;
    }

    // Gets current lat/lng
    findMe() {
        navigator.geolocation.getCurrentPosition((position) => {
            this.updateMap(position);
            console.log('find', position);
        });
    }

    // Triggered on lat/lng change
    trackMe() {
        navigator.geolocation.watchPosition((position) => {
            this.positions.push(position);
            this.updateMap(position);
            this.origin = {
                lat: this.positions[0].coords.latitude,
                lng: this.positions[0].coords.longitude
            };
            this.destination = {
                lat: this.positions.slice(-1)[0].coords.latitude,
                lng: this.positions.slice(-1)[0].coords.longitude
            };
            console.log('track', position, this.positions);
        });
    }

    updateMap(position) {
        this.currentLat = position.coords.latitude;
        this.currentLong = position.coords.longitude;
        this.latlngBounds = new window['google'].maps.LatLngBounds();
        this.latlngBounds.extend(new window['google'].maps.LatLng(position.coords.latitude, position.coords.longitude));
        this.maxZoom = 14;
        this.map.fitBounds(this.latlngBounds);
    }

}
