import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { ServiceWorkerModule } from '@angular/service-worker';

// Material
import { MaterialModule } from './material.module';

// Angularfire
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

// Angular google maps
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GeolocationComponent } from './geolocation/geolocation.component';
import { ImageComponent } from './image/image.component';

import { environment } from '../environments/environment';

// Routes
import { AppRoutingModule } from './app-routing.module';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        GeolocationComponent,
        ImageComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        MaterialModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireStorageModule,
        ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAEvHAKtBLIWyLxTx0j-IH8GsfYOnMSqqk'
        }),
        AgmDirectionModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
